WheelView
=========

在Author: wangjie基础上做了修改，多个wheel动态更新及其一些操作方式方法，详细使用办法整理完成后放上来；

去除了日志、注解方式开发等依赖关系；

数据在300条以内，再多还存在问题，还需要优化。

如图


<img src='http://git.oschina.net/uploads/images/2015/0107/104422_7849d5cb_9145.png'/>

【补充内容************************************】


Android滚动选择控件


<img src='https://raw.githubusercontent.com/wangjiegulu/WheelView/master/image/image01.png' height='500px'/>
<img src='https://raw.githubusercontent.com/wangjiegulu/WheelView/master/image/image02.png' height='100px'/>
<img src='https://raw.githubusercontent.com/wangjiegulu/WheelView/master/image/image03.png' height='100px'/>




以下仅供参考

    实现Android竖直滚动选择功能
        
      使用方式：
     
      public class MainActivity extends AIActivity {
          public static final String TAG = MainActivity.class.getSimpleName();
      
          private static final String[] PLANETS = new String[]{"河北省", "北京市", "Earth", "Mars", "Jupiter", "Uranus", "Neptune", "Pluto","其他"};
      
          @AIView(R.id.main_wv)
          private WheelView wva;
      
          @Override
          public void onCreate(Bundle savedInstanceState) {
              super.onCreate(savedInstanceState);
              wva.setOffset(1);// 偏移量，上下各一个
              wva.setItems(Arrays.asList(PLANETS));
              wva.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
                  @Override
                  public void onSelected(int selectedIndex, String item) {
                      Logger.d(TAG, "selectedIndex: " + selectedIndex + ", item: " + item);
                  }
              });
      
      
          }
          
          /**
          	* onClickCallbackSample注册监听可以下载layout的xml中
          	    <Button
			        android:id="@+id/main_show_dialog_btn"
			        android:layout_width="match_parent"
			        android:layout_height="wrap_content"
			        android:layout_margin="8dp"
			        android:onClick="onClickCallbackSample"
			        android:text="show WheelView in Dialog!" />
        	*
          	*/
          public void onClickCallbackSample(View view) {
              switch (view.getId()) {
                  case R.id.main_show_dialog_btn:
                      View outerView = LayoutInflater.from(context).inflate(R.layout.wheel_view, null);
                      WheelView wv = (WheelView) outerView.findViewById(R.id.wheel_view_wv);
                      wv.setOffset(2);
                      wv.setItems(Arrays.asList(PLANETS));
                      wv.setSeletion(3);
                      wv.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
                          @Override
                          public void onSelected(int selectedIndex, String item) {
                              Logger.d(TAG, "[Dialog]selectedIndex: " + selectedIndex + ", item: " + item);
                          }
                      });
      
                      new AlertDialog.Builder(context)
                              .setTitle("WheelView in Dialog")
                              .setView(outerView)
                              .setPositiveButton("OK", null)
                              .show();
      
                      break;
              }
          }


    } 
    
            
