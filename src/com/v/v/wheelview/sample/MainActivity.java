package com.v.v.wheelview.sample;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.v.v.wheelview.R;
import com.v.v.wheelview.WheelView;

public class MainActivity extends Activity {
    public static final String TAG = MainActivity.class.getSimpleName();

    private static final String[] PLANETS = 
    		new String[]{"北京市", "河北省", 
    		"Earth", "Mars", "Jupiter", 
    		"Uranus", "Neptune", "其他"};

    private WheelView wva,wva2;
    private String currentItemName = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);
        
        List<String> list = new ArrayList<String>();
        list.add("205T");
        list.add("北京市");
        list.add("河北省");
        for (int i = 0; i <= 200; i++) {
        	list.add("内蒙古" + (i / 2) + "中文字符-" + i);
		}
        list.add("其他");
        
        // *默认选中第一个（处理如果不滑动，就将第一个设为默认切取第一个的值），
        wva = (WheelView) findViewById(R.id.main_wv);
        wva.setOffset(1);
//        wva.setItems(Arrays.asList(PLANETS));
        wva.setItems(list);
        wva.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
//                Logger.d(TAG, "selectedIndex: " + selectedIndex + ", item: " + item);
            	Log.i("steve", "selectedIndex: " + selectedIndex + ", item: " + item);
            	currentItemName = item;
            }
        });


        // wva2
        wva2 = (WheelView) findViewById(R.id.main_wv_2);
        
        wva2.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
//                Logger.d(TAG, "selectedIndex: " + selectedIndex + ", item: " + item);
            	Log.i("steve", "右边selectedIndex: " + selectedIndex + ", item: " + item);
            }
        });
    }

    public void onClickCallbackSample(View view) {
        switch (view.getId()) {
		case 0:
			
			break;
		 case R.id.main_show_dialog_btn:
             View outerView = LayoutInflater.from(this).inflate(R.layout.wheel_view, null);
             WheelView wv = (WheelView) outerView.findViewById(R.id.wheel_view_wv);
             wv.setOffset(2);
             wv.setItems(Arrays.asList(PLANETS));
             wv.setSeletion(3);
             wv.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
                 @Override
                 public void onSelected(int selectedIndex, String item) {
//                     Logger.d(TAG, "[Dialog]selectedIndex: " + selectedIndex + ", item: " + item);
                 	Log.i("steve", "[Dialog]selectedIndex: " + selectedIndex + ", item: " + item);
                 }
             });

             new AlertDialog.Builder(this)
                     .setTitle("WheelView in Dialog")
                     .setView(outerView)
                     .setPositiveButton("OK", null)
                     .show();

             break;

		default:
			break;
		}
    }

    public void onClickUpdateData(View view){
    	Log.i("steve", "更新数据...");
    	wva2.clearItems();
    	if(currentItemName.equals("北京市")){
    		updateRightData();
    	}else{
    		List<String> list = new ArrayList<String>();
    		list.add("其他省市");
    		wva2.setItems(list);
    	}
    }
    
    private void updateRightData(){
    	List<String> list = new ArrayList<String>();
    	list.add("海淀区");
    	list.add("朝阳区");
    	list.add("昌平区");
    	list.add("西城区");
    	list.add("东城区");
    	wva2.setItems(list);
    }

}
